<?php

class TrackingModel extends Model {
    function __construct () {
        parent::__construct();
    }
    public function getClicks ($queryData) {
        $sql = "select http_useragent from clicks_tracking_test";
        $sql.= " where id_campaign = '";
        $sql.= mysqli_real_escape_string($this->connection,$queryData['id_campaign']);
        $sql.= "' and created_at = '";
        $sql.= mysqli_real_escape_string($this->connection,$queryData['created_at'])."' ";
        $sql.= " order by created_at desc";
        $sql.= " limit ".mysqli_real_escape_string($this->connection,$queryData['top'])."";
        return $this->query ($sql);
    }
    public function getCampaigns () {
        $sql = "select id_campaign from clicks_tracking_test group by id_campaign";
        return $this->query ($sql);
    }
}
