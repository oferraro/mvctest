<?php

class Model {
    protected $connection = false;
    function __construct () {
        $this->connection = (mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD)) or die(mysqli_error());
        mysqli_select_db($this->connection, DB_DATABASE) or die(mysqli_error());
    }
    function query ($sql) {
        $return = false;
        if ($res = mysqli_query ($this->connection, $sql)) {
            $result = [];
            while ($row = mysqli_fetch_object($res)) {
                $result[] = $row;
            }
            return ($result);
        }
    }
}
