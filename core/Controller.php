<?php

class Controller {
    public $viewsPath = '../views/';
    public $db = false;

    public function loadView ($viewFile, $data=[]) {
        $viewFile = $this->viewsPath.$viewFile.'.php';
        if (file_exists($viewFile)) {
            extract($data);
            ob_start();
            $strView = ob_get_contents();
            ob_end_clean();
            require ($viewFile);
        }
    }

    public function loadModel ($m) {
        if (file_exists("../models/$m.php")) {
            require_once ('../core/Model.php');
            require_once ("../models/$m.php");
            $this->db = new $m ();
        }
    }

    public function error404 ($data = []) {
        $this->loadView ('error_404', $data);
    }

}
