<?php
    require ("../views/common/head.php");
    function getPost ($name, $val) {
        return (isset($_POST[$name])&&$_POST[$name]==$val)?true:false;
    }
 ?>
<div class="container">

<form method="post">
    <div class="row">
        <div class="col-md-3">
            <label for="top">How many:</label>

            <select name="top" id="top" class="form-control">
                <option>5</option>
                <option <?php echo getPost('top',10)?' selected="selected" ':''; ?>>10</option>
                <option <?php echo getPost('top',20)?' selected="selected" ':''; ?>>20</option>
                <option <?php echo getPost('top',50)?' selected="selected" ':''; ?>>50</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="datepicker">Date:</label>
            <input type="text" autocomplete="off" name="date" id="datepicker" class="form-control" value="<?php
                echo isset($_POST['date'])?$_POST['date']:''; ?>">
        </div>
        <div class="col-md-3">
            <label for="campaign">Campaign:</label>
            <select name="campaign" id="campaign" class="form-control">
                <?php foreach ($campaigns as $c): ?>
                    <option value=<?php
                        echo $c->id_campaign;
                        ?>  <?php
                        echo getPost('campaign',$c->id_campaign)?' selected="selected" ':'';
                        ?>>Campaign <?php
                        echo $c->id_campaign; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 separator">
            <input type="submit" value="get" class="btn btn-success">
        </div>
    </div>
</form>
    <?php
    if (isset($clicks)) :
        foreach ($clicks as $c): ?>
            <div>
                <?php echo $c->http_useragent; ?>
            </div>
        <?php
        endforeach;
    endif; ?>
</div>
<script>
  $(function() {
    $("#datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
  });
</script>
<?php
require ("../views/common/footer.php"); ?>
