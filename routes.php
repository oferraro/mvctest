<?php

switch (USER_URL) {
    case '/home':
        if ($controller = loadController('HomeController')) {
            $controller->index();
        }
        break;
    default:
        if ($controller = loadController('Controller', '../core/')) {
            $controller->error404();
        }
}

function loadController ($c, $path = '../controllers/') {
    $controllersRoute = getcwd()."/$path";
    if (file_exists($controllersRoute."$c.php")) {
        require_once ("$controllersRoute$c.php");
        $controller = new $c ();
        return $controller;
    } else {
        return false;
    }
}
