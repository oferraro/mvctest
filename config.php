<?php

$route = explode (DIRECTORY_SEPARATOR, $_SERVER['SCRIPT_NAME']);
unset($route[count($route)-1]);
define ('BASE_PATH', implode(DIRECTORY_SEPARATOR, $route));
$userRoute = rtrim (trim(str_replace (BASE_PATH, '', $_SERVER['REQUEST_URI'])), '/');
define ('USER_URL', $userRoute);

define ('DB_HOST',        'localhost');
define ('DB_USER',        'testmarbella');
define ('DB_PASSWORD',    'testuserpass');
define ('DB_DATABASE',    'tracking');
