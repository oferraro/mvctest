<?php

class HomeController extends Controller {
    public function index () {
        $this->loadModel ('TrackingModel');
        if (count($_POST)>0) {
            $post = $_POST;
            $queryData['id_campaign']   = $post['campaign'];
            $queryData['created_at']    = $post['date'];
            $queryData['top']           = $post['top'];
            $data['clicks'] = $this->db->getClicks($queryData);
        }
        $data['campaigns'] = $this->db->getCampaigns();
        $this->loadView ('home', $data);
    }
}
